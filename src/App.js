import React, { Component } from 'react';
import { distanceInWordsToNow } from 'date-fns';

// Helpers
import { fetchShortenedUrl } from './shared/utils/helper.js';
import { findIndex, isEqual } from 'lodash'

// Components
import LinkShortenerInput from './shared/components/link-shortener-input/LinkShortenerInput.js';
import PreviousList from './shared/components/previous-list/PreviousList.js';

// Styles
import styles from './App.css';

class App extends Component {
	constructor() {
		super();
		this.state = {
			isLoading: false,
			history: [{
				shortenedUrl: null,
				originalUrl: null,
				totalVisits: null,
				lastVisited: {
					message: null,
					timeStamp: null,
				},
			}],
			isHighlighted: false,
		};
	}

	/**
	 * Check if there's already anything stored in localStorage and add it to the
	 * local state otherwise it creates an empty array
	*/
	componentWillMount() {
		const history = JSON.parse(localStorage.getItem('history'));

		if (history) {
			history.map((item, index) => {
				if (item.lastVisited.timeStamp) {
					return item.lastVisited.message = distanceInWordsToNow(item.lastVisited.timeStamp);
				}

				return item.lastVisited.message;
			});
		}

		return this.setState({
			history: history || [],
		});
	}

	/**
	 * Update the localStorage each time there's a change in the 'history'
	 * property of the component's state
	*/
	componentDidUpdate() {
		if (!isEqual(this.state.history, JSON.parse(localStorage.getItem('history')))) {
			localStorage.setItem('history', JSON.stringify(this.state.history));
		}
	}

	render() {
		return(
			<div className={ styles.App }>
				<header className={ styles.header }>
					<h1 className={ styles.title }>Shooooort</h1>
					<h2 className={ styles.subtitle }>The link shortener with a long name</h2>
				</header>
				<LinkShortenerInput submitUrl={ this.submitUrl } />
				<PreviousList
					isHighlighted={ this.state.isHighlighted }
					history={ this.state.history }
					onLinkClick={ this.onLinkClick }
					clearHistory={ this.clearHistory } />
			</div>
		);
	}

	/**
	 * Run the helper that makes the API request for the shortened URL
	 * and add the results to the state and localStorage
	*/
	submitUrl = async (url) => {
		const history = this.state.history;

		return await fetchShortenedUrl(url).then((res) => {
			history.unshift({
				shortenedUrl: res.shortcode,
				originalUrl: url,
				totalVisits: 0,
				lastVisited: {
					message: null,
					timeStamp: null,
				},
			});

			return this.setState({
				history,
				isHighlighted: true,
			});

		});
	}

	/**
	 * Clears the url history by replacing the decorated array with an empty one
	*/
	clearHistory = () => {
		this.setState({
			history: [],
		});
	}

	/**
	 * Increment the 'totalVisits' property whenever the user user clicks a
	 * shortened link
	*/
	onLinkClick = (url) => {
		const entryIndex = findIndex(this.state.history, (item) => item.shortenedUrl === url);

		this.setState((prevState) => {
			const listedItem = prevState.history[entryIndex];
			const { timeStamp, message } = this.newDate();

			listedItem.totalVisits += 1;
			listedItem.lastVisited.message = message;
			listedItem.lastVisited.timeStamp = timeStamp;

			return prevState;
		});
	}

	/**
	 * Returns an object containg a timestamp of the current date and
	 * a message
	*/
	newDate = () => {
		const currentDate = new Date();

		return {
			timeStamp: currentDate,
			message: 'less than a minute',
		};
	}
};

export default App;
