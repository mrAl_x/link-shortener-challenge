import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Styles
import styles from './LinkShortenerInput.css';

class LinkShortenerInput extends Component {
	constructor() {
		super();
		this.state = {
			url: null,
		};

		this.textInput = null;
	}

	render() {
		const hasUrl = (this.state.url && this.state.url.length > 0);

		return (
			<div className={ styles.LinkShortenerInput }>
				<input type="text"
					className={ styles.input }
					aria-label="Type or paste link to shorten"
					onChange={ (e) => this.handleOnChange(e) }
					placeholder="https://"
					ref={ (ref) => this.textInput = ref } />

				<input type="button"
					className={ styles.button }
					disabled={ !hasUrl }
					onClick={ hasUrl && this.handleButtonClick }
					value="Shorten this link" />
			</div>
		);
	}

	/**
	 * Saves the input value to be sent to the parent - via callback function - on
	 * submit
	*/
	handleOnChange = (e) => {
		this.setState({
			url: e.target.value || null,
		});
	}

	/**
	 * Executes a callback function so the parent can make a request to the API;
	 * Clears the component's state and the input text field
	*/
	handleButtonClick = () => {
		this.props.submitUrl(this.state.url);
		this.setState({
			url: null,
		});
		this.textInput.value = null;
	}
};

LinkShortenerInput.propTypes = {
	submitUrl: PropTypes.func.isRequired,
};

export default LinkShortenerInput;
