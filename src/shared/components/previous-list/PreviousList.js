import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Components
import ListItem from './list-item/ListItem.js'

// Styles
import styles from './PreviousList.css';

class PreviousList extends Component {
	render() {
		const { history, isHighlighted, clearHistory } = this.props;

		return (
			<div className={  styles.PreviousList  }>
				<h3 className={  styles.title  }>
					Previously shortened by you
				</h3>
				{
					history.length > 0 && <input type="button"
						title="Clear history"
						value="Clear history"
						onClick={ clearHistory }
						className={  styles.clearHistoryButton  } />
				}

				<table className={  styles.table  }>
					<thead className={  styles.tableHeader  }>
						<tr className={  styles.tableRow  }>
							<th className={  styles.tableHead  }>Link</th>
							<th className={  styles.tableHead  }>Visits</th>
							<th className={  styles.tableHead  }>Last Visited</th>
						</tr>
					</thead>
					<tbody>
						{
							history.length > 0 && history.map((item, index) => (
								<ListItem
									key={ index }
									isEntryHighlighted={ isHighlighted }
									item={ item }
									onLinkClick={ this.onLinkClick }
									index={ index } />
							))
						}
					</tbody>
				</table>

			</div>
		);
	}

	/**
	 * Callback function sending the shortened URL - to serve as
	 * reference - back to the parent
	*/
	onLinkClick = (url) => {
		this.props.onLinkClick(url);
	}
};

PreviousList.defaultProps = {
	isEntryHighlighted: false,
}

PreviousList.propTypes = {
	clearHistory: PropTypes.func.isRequired,
	onLinkClick: PropTypes.func.isRequired,
	history: PropTypes.array,
	isEntryHighlighted: PropTypes.bool,
};

export default PreviousList;
