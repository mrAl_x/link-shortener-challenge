import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import Copy from 'copy-to-clipboard';

// Styles
import styles from '../PreviousList.css';

class ListItem extends Component {
	constructor() {
		super();
		this.state = {
			ariaMessage: null,
		};

		this.apiUrl = 'https://impraise-shorty.herokuapp.com/';
	};

	render() {
		const { isEntryHighlighted, item, index } = this.props;

		return (
			<tr key={ index }
				className={ styles.tableRow }>
				<td className={ classnames(
					styles.tableCell,
					{ [styles.tableCell_highlight]: (index === 0 && isEntryHighlighted) }
					) }>
					<div className={ styles.shortenedLink }>
						<a href={ `${ this.apiUrl }${ item.shortenedUrl }` }
							target="_blank"
							aria-label="Shortened url"
							onClick={() => this.handleLinkClick(item.shortenedUrl) }>
							shooooort.com/
							<span className={ styles.shortenedLink_newUrl }>
								{ item.shortenedUrl }
							</span>
						</a>
						<input type="button"
							className={ styles.clickToCopyButton }
							onClick={ () => this.handleCopyButton(item.shortenedUrl) }
							value="Click to copy this link" />
						<p className={ styles.shortenedLink_oldUrl }>
							{ item.originalUrl }
						</p>
						<span className={ styles.ariaMessage }
							aria-live="assertive">
							{ this.state.ariaMessage }
						</span>
					</div>
				</td>
				<td className={ styles.tableCell }>
					{ item.totalVisits || 0 }
				</td>
				<td className={ styles.tableCell }>
					{
						(item.lastVisited && item.lastVisited.message)
							? `${item.lastVisited.message} ago`
							: 'Never'
					}
				</td>
			</tr>
		);
	};

	/**
	 * Callback function sending the shortened URL - to serve as
	 * reference - back to the parent
	*/
	handleLinkClick = (shortenedUrl) => {
		this.props.onLinkClick(shortenedUrl);
	}

	/**
	 * Copies the newly shortened URL to the clipboard and updates the component's
	 * state so the screen reader returns feedback
	*/
	handleCopyButton = (shortenedUrl) => {
		Copy(`${this.apiUrl}${shortenedUrl}`);
		this.setState({
			ariaMessage: 'Shortened url copied to clipboard'
		});
	};
};

ListItem.propTypes = {
	index: PropTypes.number.isRequired,
	item: PropTypes.object.isRequired,
	onLinkClick: PropTypes.func.isRequired,
	isEntryHighlighted: PropTypes.bool,
};

export default ListItem;
