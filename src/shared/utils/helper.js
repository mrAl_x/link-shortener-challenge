export async function fetchShortenedUrl(url) {
	const corsAnywhere = 'https://cors-anywhere.herokuapp.com/';
	const apiUrl = 'https://impraise-shorty.herokuapp.com/shorten';
	const data = { url };

	try {
		return await fetch(`${corsAnywhere}${apiUrl}`,
			{
				body: JSON.stringify(data),
				method: 'POST',
			}
		).then((res) => res.json().then((data) => data));
	} catch (error) {
		console.error(error);
		return false;
	}
}
