# Link Shortner Challenge
This is a simple app, using **React** that shortens a URL given by a user.

The build system is implemented using **Webpack**.

This application allows the user to:

* Submit a url to be shortened
* Keep and clear a record of all the formerly submitted URLs
* Copy each shortened link to the clipboard

## Running the project

### Installation
In order to run this app you'll need to install its dependencies via **NPM**:

`$ npm install`

### Build
To build the project you can run:

`$ npm run build`

The bundled project should be under the `/build` folder in the root of the project.

### Development Server
To startup a local server you can run:

`$ npm start`

### A11Y
This app is fully accessible so feel free to explore it with the keyboard.

Plus if you're running **MacOS**, try pressing *CMD + F5* to enable the screen reader :)